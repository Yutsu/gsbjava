package com.example.gsb;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.gsb.entities.DateVisite;
import com.example.gsb.entities.Praticien;
import com.example.gsb.technique.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.R.layout.simple_spinner_item;

public class SaisirCompteRendu extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private Button btnDate;
    private TextView txtDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private Spinner spinnerPrac;
    private ArrayList<Praticien> lesPraticiens = new ArrayList<>();


    /**
     * Permet d'obtenir le date picker et faire appel au web service pour récupérer
     * la liste des praticiens selon l'utilisateur
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisir_compte_rendu);
        btnDate = (Button)findViewById(R.id.idButton);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new com.example.gsb.entities.DatePicker();
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });

        String matricule = Session.getLeVisiteur().getVisMatricule();
        String url = "http://192.168.56.1/gsbandroid/web/app.php/praticien/" + matricule;
        Response.Listener<JSONArray> responseListener; // JSONArray -> LISTE
        responseListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        String nomPraticien = response.getJSONObject(i).getString("pra_nom"); //JSONObject -> 1 valeur
                        String prenomPraticien = response.getJSONObject(i).getString("pra_prenom"); //JSONObject -> 1 valeur
                        Praticien lePraticien = new Praticien(nomPraticien, prenomPraticien);
                        lesPraticiens.add(lePraticien);
                    }
                    afficherLesPraticiens(lesPraticiens);
                    Log.i("APP_RV", "Date rapport : " + response.toString());

                } catch (JSONException e) {
                    Log.e( "APP-RV" , "Erreur JSON : " + e.getMessage() ) ;
                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur JSON : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.POST, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }


    /**
     * Permet d'obtenir un calendrier dans lequel le visiteur choisi
     * une date pour la date de rapport
     * @param view
     * @param year
     * @param month
     * @param dayOfMonth
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.YEAR, year);
        calender.set(Calendar.MONTH, month);
        calender.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String currentDateString = DateFormat.getDateInstance(DateFormat.SHORT).format(calender.getTime());

        TextView txtDate = (TextView) findViewById(R.id.txtDate);
        txtDate.setText(currentDateString);

    }

    /**
     * Affiche la liste des praticien dans le spinner
     * @param lesPraticiens
     */
    private void afficherLesPraticiens(ArrayList<Praticien> lesPraticiens) {
        ArrayAdapter<Praticien> adaptateur = new ArrayAdapter<Praticien>(this,
                simple_spinner_item ,lesPraticiens); //mettre toString
        adaptateur.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerPrac.setAdapter(adaptateur) ;
    }

    public void validerSaisie(View view) {
        Intent retourMenu = new Intent(this, Activite_secondaire.class);
        startActivity(retourMenu);
    }

    /**
     * Retourne à la page précédente dans lequel il y a le menu avec les
     * 3 boutons
     * @param view
     */
    public void retour(View view) {
        Intent retourMenu = new Intent(this, Activite_secondaire.class);
        startActivity(retourMenu);
    }

}
