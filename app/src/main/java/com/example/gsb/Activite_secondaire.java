package com.example.gsb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.gsb.entities.DateVisite;
import com.example.gsb.technique.Session;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class Activite_secondaire extends AppCompatActivity {

    TextView txtMenu;
    String nom = Session.getLeVisiteur().getVisNom();
    String prenom = Session.getLeVisiteur().getVisPrenom();
    private ArrayList<String> listRapportDate = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activite_secondaire);
        txtMenu = (TextView)findViewById(R.id.txtMenu);
        txtMenu.setText("Menu du visiteur : " + nom + " " + prenom );
    }

    /**
     * Permet d'aller à la prochaine page en cliquant sur le bouton
     * "Saisir un compte rendu"
     * @param view
     */
    public void saisirCompteRendu(View view) {
        Intent intentSaisirCompteRendu = new Intent(this, SaisirCompteRendu.class);
        startActivity(intentSaisirCompteRendu);
    }

    /**
     * Permet d'aller à la prochaine page en cliquant sur le bouton
     * "Consulter un compte rendu"
     * De plus, on fait appel au web service en allant récupérer les dates de rapport en fonction du visiteur
     * @param view
     */
    public void consulterCompteRendu(View view) {

        String matricule = Session.getLeVisiteur().getVisMatricule();
        String url = "http://192.168.56.1/gsbandroid/web/app.php/lesCR/" + matricule;

        Response.Listener<JSONArray> responseListener; //JSONObject -> 1 valeur
        responseListener = new Response.Listener<JSONArray>() { //LISTE
            @Override
            public void onResponse(JSONArray response) {
                try {
                    //boolean identique;

                    for (int i = 0; i < response.length(); i++) {
                        String rapDateRapport = response.getJSONObject(i).getString("rapDateRapport");
                        //DateVisite dateVisite = new DateVisite(rapDateRapport);
                        //identique = false; //Si jamais il y a 2 dates identiques
                        /*
                        for(DateVisite uneDateVisite : listRapportDate){
                            if(uneDateVisite.equals(dateVisite)){
                                identique = true;
                            }
                        }
                        if(identique == false){
                            listRapportDate.add(dateVisite);
                        }*/
                        listRapportDate.add(rapDateRapport);

                        Intent intentConsulterCompteRendu = new Intent(Activite_secondaire.this, ConsulterCompteRendu.class);
                        intentConsulterCompteRendu.putExtra("Dates", listRapportDate);
                        startActivity(intentConsulterCompteRendu);
                    }

                    Log.i("APP_RV", "Date rapport : " + response.toString());
                } catch (JSONException e) {
                    Log.e( "APP-RV" , "Erreur JSON : " + e.getMessage() ) ;
                }
            }
        };


        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur JSON : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);


    }

    /**
     * Permet d'aller à la prochaine page en cliquant sur le bouton
     * "Consulter tous les comptes rendus"
     * @param view
     */
    public void consulterToutCompteRendu(View view) {
        Intent intentAfficherTout = new Intent(this, ConsulterToutCompteRendu.class);
        startActivity(intentAfficherTout);
    }

    /**
     * Permet à l'utilisateur de se déconnecter de son compte
     * La page renverra alors à la page de connexion
     * @param view
     */
    public void seDeconnecter(View view) {
        Session.getSession().fermer();
        Intent retourConnexion = new Intent(this, MainActivity.class);
        startActivity(retourConnexion);
    }
}
