package com.example.gsb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.gsb.entities.Visiteur;
import com.example.gsb.technique.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class MainActivity extends AppCompatActivity {
    public static final String TAG="MainActivity";
    EditText editLogin;
    EditText editPassword;
    Button btnValider;
    Button btnEffacer;
    Visiteur visiteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    /**
     * Permet d'initialiser les composants
     */
    public void init() {
        editLogin = (EditText) findViewById(R.id.editLogin);
        editPassword = (EditText) findViewById(R.id.editPassword);
        btnValider = (Button) findViewById(R.id.btnValider);
        btnEffacer = (Button) findViewById(R.id.btnEffacer);
    }

    /**
     * Permet au visiteur de s'identifier sur son compte
     * Si le mot de passe ou le login est incorrect, un message d'erreur s'affichera
     * @param vue
     */
    public void valider(View vue) {
        String login = editLogin.getText().toString();
        String mdp = editPassword.getText().toString();
        String url = "http://192.168.56.1/gsbandroid/web/app.php/connexion/" + login + "/" + mdp;

        Response.Listener<JSONObject> responseListener;
        responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String matricule = response.getString("visMatricule"); //variable symfony
                    String nom = response.getString("visNom");
                    String prenom = response.getString("visPrenom");

                    Toast.makeText(MainActivity.this, "Connexion réussie : "
                            + matricule + " "+ nom + " " + prenom, Toast.LENGTH_LONG).show();
                    Log.i("APP_RV", "Visiteur : " + response.toString());

                    connecterVisiteur(matricule, nom, prenom);

                } catch (JSONException e) {
                    if (Session.getSession() != null) {
                        Session.getSession().fermer();
                    }
                    Toast.makeText(MainActivity.this, "Echec de connexion ",
                            Toast.LENGTH_LONG).show();
                    Log.e("APP-RV", "Erreur : " + e.getMessage());
                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur JSON : " + error.getMessage());
            }
        };
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    /**
     * Permet d'aller à la prochaine page si le login et mot de passe sont corrects ou non si
     * le visiteur s'est trompé en tapant
     * @param matricule
     * @param nom
     * @param prenom
     */
    public void connecterVisiteur(String matricule, String nom, String prenom) {
        visiteur = new Visiteur();
        visiteur.setVisMatricule(matricule);
        visiteur.setVisNom(nom);
        visiteur.setVisPrenom(prenom);
        if (!(Session.init(visiteur))) {
            Log.e("Main activity","Erreur session");
        } else {
            Intent intentEnvoyerMenu = new Intent(this, Activite_secondaire.class);
            startActivity(intentEnvoyerMenu);
        }

    }

    /**
     * Permet à l'utilisateur de pouvoir effacer ce qu'il a écrit dans le login et mot de passe
     * @param view
     */
    public void reinitialiser(View view) {
        // Action sur clic du bouton Annuler
        editLogin.setText("");
        editPassword.setText("");
        if (Session.getSession() != null) {
            Session.getSession().fermer();
        }
    }
}

