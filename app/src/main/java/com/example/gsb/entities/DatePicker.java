package com.example.gsb.entities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

public class DatePicker extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState){
        Calendar calender = Calendar.getInstance();
        int annee = calender.get(Calendar.YEAR);
        int mois = calender.get(Calendar.MONTH);
        int jour = calender.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), annee, mois, jour);
    }

}
