package com.example.gsb.entities;

public class DateVisite {
    private String dateRapport ;

    public DateVisite (String dateRapport){
        this.dateRapport = dateRapport;
    }

    public String getDateRapport(){
        return dateRapport;
    }

    public void setDateRapport(String dateRapport){
        this.dateRapport = dateRapport;
    }

    public String toString(){
        return getDateRapport();
    }



}
