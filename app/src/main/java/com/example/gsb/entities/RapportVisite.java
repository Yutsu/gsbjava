package com.example.gsb.entities;

import java.util.Date;

public class RapportVisite {
    private int rapNum;
    private String rapBilan;
    private String rapDateRapport;
    private String rapDateVisite;
    private String nomPraticien;
    private String prenomPraticien;


    public RapportVisite(int rapNum, String rapBilan, String rapDateRapport,
                         String rapDateVisite, String nomPraticien, String prenomPraticien) {
        this.rapNum = rapNum;
        this.rapBilan = rapBilan;
        this.rapDateRapport = rapDateRapport;
        this.rapDateVisite = rapDateVisite;
        this.nomPraticien = nomPraticien;
        this.prenomPraticien = prenomPraticien;
    }

    public RapportVisite(String rapDaterapport){
        this.rapDateRapport = rapDaterapport;
    }

    public int getRapNum() {
        return rapNum;
    }

    public String getrapBilan() {
        return rapBilan;
    }

    public String getRapDaterapport() { return rapDateRapport; }

    public String getRapDateVisite() { return rapDateVisite; }

    public String getNomPraticien() { return nomPraticien; }

    public String getPrenomPraticien() { return prenomPraticien; }

    public void setRapNum(int rapNum){
        this.rapNum = rapNum;
    }

    public void setRapBilan(String rapBilan){
        this.rapBilan = rapBilan;
    }

    public void setRapDaterapport(String rapDaterapport) { this.rapDateRapport = rapDaterapport; }

    public void setRapDateVisite(String rapDateVisite) { this.rapDateVisite = rapDateVisite; }

    public void setNomPraticien(String nomPraticien){
        this.nomPraticien = nomPraticien;
    }

    public void setPrenomPraticien(String prenomPraticien){
        this.prenomPraticien = prenomPraticien;
    }


    public String toString() {
        return "Rapport numéro : " + getRapNum() + "\n \n" +
                "Rapport bilan : " + getrapBilan() + "\n \n" +
                "Date rapport : " + getRapDaterapport() + "\n \n" +
                "Date visite : " + getRapDateVisite() + "\n \n" +
                "Nom praticien : " + getNomPraticien() + "\n \n" + //Normalement c'est nom.getPraNom() ...
                "Prénom praticien : " + getPrenomPraticien() + "\n";}

}

