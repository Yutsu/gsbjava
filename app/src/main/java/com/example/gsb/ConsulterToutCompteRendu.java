package com.example.gsb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.gsb.entities.RapportVisite;
import com.example.gsb.technique.Session;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ConsulterToutCompteRendu extends AppCompatActivity {

    private ListView listView; //l.75
    private ArrayList<RapportVisite> listRapport = new ArrayList<>();

    /**
     * Permet d'afficher la liste de tous les comptes rendu du visiteur
     * en faisant appel au web service
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {    
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_tout_compte_rendu);
        listView = (ListView) findViewById(R.id.listRendus);

        String matricule = Session.getLeVisiteur().getVisMatricule();
        String url = "http://192.168.56.1/gsbandroid/web/app.php/lesCR/" + matricule;

        Response.Listener<JSONArray> responseListener; //JSONObject -> 1 valeur
        responseListener = new Response.Listener<JSONArray>() { //LISTE
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {

                        int rapNum = response.getJSONObject(i).getInt("rapNum");
                        String rapBilan = response.getJSONObject(i).getString("rapBilan");
                        String rapDaterapport = response.getJSONObject(i).getString("rapDateRapport");
                        String rapDateVisite = response.getJSONObject(i).getString("rapDateVisite");
                        String nomPraticien = response.getJSONObject(i).getString("praNom");
                        String prenomPraticien = response.getJSONObject(i).getString("praPrenom");

                        RapportVisite rapportVisite = new RapportVisite(rapNum, rapBilan, rapDaterapport, rapDateVisite, nomPraticien, prenomPraticien);
                        listRapport.add(rapportVisite);
                    }
                    afficherlesCR(listRapport);
                    Log.i("APP_RV", "CompteRendu : " + response.toString());
                } catch (JSONException e) {
                    Log.e( "APP-RV" , "Erreur JSON : " + e.getMessage() ) ;
                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur JSON : " + error.getMessage());
            }
        };

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    /**
     * Affiche la liste des comptes rendus sur la liste avec ArrayAdapter
     * @param listCompteRendu
     */
    private void afficherlesCR(ArrayList<RapportVisite> listCompteRendu){
            ArrayAdapter<RapportVisite> adaptateur = new ArrayAdapter<RapportVisite>(this,
                    android.R.layout.simple_list_item_1 ,listCompteRendu); //mettre toString
            listView.setAdapter(adaptateur) ;
    }

    /**
     * Retourne à la page précédente dans lequel il y a le menu avec les
     * 3 boutons
     * @param view
     */
    public void retourMenu(View view) {
        Intent retourMenu = new Intent(this, Activite_secondaire.class);
        startActivity(retourMenu);
    }
}
