package com.example.gsb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.gsb.entities.DateVisite;
import com.example.gsb.entities.Praticien;
import com.example.gsb.entities.RapportVisite;
import com.example.gsb.technique.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.R.layout.simple_spinner_item;

public class ConsulterCompteRendu extends AppCompatActivity {

    private Spinner spinner;
    private ListView listViewRendue;
    private ArrayList<String> listRapportDate = new ArrayList<>();
    private ArrayList<RapportVisite> listVisite = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_compte_rendu);
        init();

        Bundle paquet = this.getIntent().getExtras();
        listRapportDate = paquet.getStringArrayList("Dates");
        Log.d( "APP-RV", ""+ listRapportDate ) ;
        //afficherLesDates(listRapportDate);

    }
    /*
    private void afficherLesDates(ArrayList<String> lesDates) {
        ArrayAdapter<String> adaptateur = new ArrayAdapter<String>(this,
                simple_spinner_item ,lesDates);
        adaptateur.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adaptateur) ;
    }*/

    public void init() {
        spinner = (Spinner) findViewById(R.id.idSpinner);
        listViewRendue = (ListView) findViewById(R.id.listRendus);
    }

    /*
    private void afficherLesDates(ArrayList<DateVisite> listCompteRendu){
        ArrayAdapter<DateVisite> adaptateur = new ArrayAdapter<DateVisite>(this,
                android.R.layout.simple_spinner_item ,listCompteRendu); //mettre toString
        adaptateur.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adaptateur) ;

        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Log.d("Date :", item.toString());

            }
        });
        //afficherLesComptesRendus(adaptateur);
    }*/

    /*
    private void afficherLesComptesRendus(ArrayAdapter<DateVisite> date) {
        ArrayAdapter<RapportVisite> adaptateur = new ArrayAdapter<RapportVisite>(this,
                android.R.layout.simple_list_item_1, (List<RapportVisite>) date);
        listViewRendue.setAdapter(adaptateur);
    }*/


    /**
     * Retourne à la page précédente dans lequel il y a le menu avec les
     * 3 boutons
     * @param view
     */
    public void retourMenu(View view) {
        Intent retourMenu = new Intent(this, Activite_secondaire.class);
        startActivity(retourMenu);
    }
}
